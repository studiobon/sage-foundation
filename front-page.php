<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/excerpt', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>

<aside class="ad-section">Ad section</aside>

<!-- ABOUT -->
<?php if($aboutPage = get_page_by_title('about')): ?>
<?php
  global $post;
  $post = $aboutPage;
  setup_postdata($post); ?>
<article class="frontpage__about entry page">
  <h2 class="entry__title">
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
  </h2>
  <div class="entry__content">
    <?php the_content(); ?>
  </div>
</article>
<?php wp_reset_postdata();?>
<?php endif; ?>

<!-- DESIGNER LIST -->
<?php $designers = get_designers(); ?>
<?php if($designers): ?>
<article class="frontpage__designer-list designer-list">
  <h2 class="designer-list__title">
    <a href="/designers">Designer A–Z</a>
  </h2>
  <div class="designer-list__content">
  <?php foreach ($designers as $post): setup_postdata( $post ); ?>
    <?php get_template_part( 'templates/excerpt', 'designers' ); ?>
  <?php endforeach; wp_reset_postdata(); ?>
  </div>
</article>
<?php endif;?>

<!-- PARTNERS -->
<?php if($partnersPage = get_page_by_title('partners')): ?>
<?php
  global $post;
  $post = $partnersPage;
  setup_postdata($post); ?>
<article class="frontpage__partners entry page">
  <h2 class="entry__title">
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
  </h2>
  <div class="entry__content">
    <?php the_content(); ?>
  </div>
</article>
<?php wp_reset_postdata();?>
<?php endif; ?>
