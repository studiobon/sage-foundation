<?php

/**
 * COLLECTIONS MODELS
 */

// Returns array of collections for specified designer
function get_collections($designer_id) {
  $collections = get_posts(array(
                'post_type' => 'collections',
                'meta_query' => array(
                  array(
                    'key' => 'designer',
                    'value' => '"' . $designer_id . '"',
                    'compare' => 'LIKE'
                  )
                )
              ));
  return $collections;
}

// Returns the gallery markup
function get_the_gallery() {
  $image_ids = get_field('gallery', false, false);
  $gallery_shortcode = '[gallery link="none" size="medium" ids="' . implode(',', $image_ids) . '"]';
  return do_shortcode( $gallery_shortcode );
}
