<?php

/**
 * EVENTS MODELS
 */

// Returns array of events for specified designer
function get_events($designer_id) {
  $events = get_posts(array(
                'post_type' => 'events',
                'meta_query' => array(
                  array(
                    'key' => 'designer',
                    'value' => '"' . $designer_id . '"',
                    'compare' => 'LIKE'
                  )
                )
              ));
  return $events;
}
