<?php

/**
 * DESIGNER MODELS
 */

function get_designers() {
  $designers_args = array(
    'post_type' => 'designers',
    'posts_per_page' => -1
   );
  $designers = get_posts($designers_args);

  return $designers;
}
