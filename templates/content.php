<article <?php post_class(); ?>>
  <header>
    <?php get_template_part('templates/entry-meta'); ?>
    <figure class="entry__image">
      <?php the_post_thumbnail(); ?>
    </figure>
    <h2 class="entry__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="entry__summary">
    <?php the_excerpt(); ?>
  </div>
  <p class="entry__read-more">Read more</p>
</article>
