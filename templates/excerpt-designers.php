<article <?php post_class(); ?>>
  <h2 class="entry__title">
    <a href="<?php the_permalink(); ?>">
      <?php the_title(); ?>
    </a>
  </h2>
  <figure class="entry__image">
    <?php the_post_thumbnail(); ?>
  </figure>
</article>
