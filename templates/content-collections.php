<article id="collection-<?php the_title(); ?>" class="designer__collection collection">
  <?php echo get_the_gallery(); ?>

  <?php if( get_field('video') ): ?>
  <div class="flex-video">
    <?php the_field('video'); ?>
  </div>
  <?php endif; ?>

  <h2 class="collection__title">
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
  </h2>
  <div class="collection__content">
    <?php the_content(); ?>
  </div>
</article>
