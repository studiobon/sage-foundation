<section <?php post_class('designer'); ?>>
  <?php if( $collections = get_collections( get_the_id() ) ): ?>
  <div class="designer__collections">
  <?php foreach ($collections as $post): setup_postdata( $post ); ?>
    <?php get_template_part( 'templates/content', 'collections' ); ?>
  <?php endforeach; wp_reset_postdata(); ?>
  </div>
  <?php endif; ?>

  <header id="designer__header" class="designer__header">
    <h1 class="designer__title">
      <a href="<?php the_permalink(); ?>">
        <?php the_title(); ?>
      </a>
    </h1>

    <?php if( $collections ): ?>
    <nav class="designer__collection-nav collection-nav">
      <ul>
      <?php foreach ($collections as $collection): ?>
        <li><a class="collection-nav__toggler" href="#collection-<?= get_the_title($collection->ID) ?>"><?= get_the_title($collection->ID) ?></a></li>
      <?php endforeach; ?>
      </ul>
    </nav>
    <?php endif; ?>

  </header><!-- /designer-header -->

  <div class="designer__about">
    <?php the_content(); ?>
  </div>

  <div class="designer__contacts">
    <?php the_field('contacts'); ?>
  </div>

  <div class="designer__downloads">
    <?php the_field('downloads'); ?>
  </div>

</section>
