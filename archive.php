<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/excerpt', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>

<?php if( is_post_type_archive('designers') ): ?>
  <?php $designers = get_designers(); ?>
  <?php if($designers): ?>
  <nav class="designers-index">
    <button type="toggle">Index</button>
    <ul class="designers-index__list">
    <?php foreach ($designers as $post): setup_postdata( $post ); ?>
      <li><?php the_title(); ?></li>
    <?php endforeach; wp_reset_postdata(); ?>
    </ul>
  </nav>
  <?php endif;?>
<?php endif; ?>
