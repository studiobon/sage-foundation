<?php

namespace Roots\Sage\PostTypes;

/**
 * Custom Post Types
 */

function create_post_types() {

  register_post_type( 'designers',
    array(
      'labels' => array(
        'name' => __( 'Designers' ),
        'singular_name' => __( 'Designer' ),
        'menu_name'          => __( 'Designers'),
        'name_admin_bar'     => __( 'Designer'),
        'add_new'            => __( 'Add New'),
        'add_new_item'       => __( 'Add New Designer'),
        'new_item'           => __( 'New Designer' ),
        'edit_item'          => __( 'Edit Designer' ),
        'view_item'          => __( 'View Designer' ),
        'all_items'          => __( 'All Designers' ),
        'search_items'       => __( 'Search Designers' ),
        'not_found'          => __( 'No Designer found.' ),
        'not_found_in_trash' => __( 'No Designer found in Trash.' )
      ),
      'menu_icon' => 'dashicons-id-alt',
      'public' => true,
      'has_archive' => true
    )
  );

  register_post_type( 'collections',
    array(
      'labels' => array(
        'name' => __( 'Collections' ),
        'singular_name' => __( 'Collection' ),
        'name_admin_bar'     => __( 'Collection'),
        'add_new'            => __( 'Add New'),
        'add_new_item'       => __( 'Add New Collection'),
        'new_item'           => __( 'New Collection' ),
        'edit_item'          => __( 'Edit Collection' ),
        'view_item'          => __( 'View Collection' ),
        'all_items'          => __( 'All Collections' ),
        'search_items'       => __( 'Search Collections' ),
        'not_found'          => __( 'No Collection found.' ),
        'not_found_in_trash' => __( 'No Collection found in Trash.' )
      ),
      'menu_icon' => 'dashicons-images-alt2',
      'public' => false,
      'show_ui' => true,
      'has_archive' => false
    )
  );

  register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'Events' ),
        'singular_name' => __( 'Event' ),
        'name_admin_bar'     => __( 'Event'),
        'add_new'            => __( 'Add New'),
        'add_new_item'       => __( 'Add New Event'),
        'new_item'           => __( 'New Event' ),
        'edit_item'          => __( 'Edit Event' ),
        'view_item'          => __( 'View Event' ),
        'all_items'          => __( 'All Events' ),
        'search_items'       => __( 'Search Events' ),
        'not_found'          => __( 'No Event found.' ),
        'not_found_in_trash' => __( 'No Event found in Trash.' )
      ),
      'menu_icon' => 'dashicons-calendar-alt',
      'public' => true,
      'has_archive' => false
    )
  );

}

add_action( 'init', __NAMESPACE__ . '\\create_post_types' );
