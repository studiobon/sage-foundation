<?php

namespace Roots\Sage\Pages;


/**
 * Create pages that theme relies on if they don't already exist
 */

// wp_insert_post wrapper for creating pages
function create_page($page) {
  global $user_ID;

  // Check for page existence
  $page_check = get_page_by_title($page['post_title']);
  if( $page_check !== NULL ) {
    $pageid = $page_check->ID;
  }
  // Create page
  else {
    $page['post_author'] = $user_ID;
    $page['post_type'] = 'page';
    $page['post_status'] = 'publish';
    $pageid = wp_insert_post($page);
  }

  // Display Error if there was a problem creating the page
  if ($pageid == 0) {
    add_action( 'admin_notices', function() use ( $page ) {
      ?>
      <div class="error">
        <p>There was a problem creating page <i><?php echo $page['post_title']; ?></i></p>
      </div>
      <?php
      }
    );
  }
  // No errors!
  else {
    // Create child pages if we have any
    if( isset($page['post_children']) ) {
      foreach ($page['post_children'] as $child) {
        $child['post_parent'] = $pageid;
        create_page($child);
      }
    }
  }
}

// Generate default pages
function generate_default_content() {

  // About us
  $about_us_page = array(
    'post_title' => 'About',
    'post_content' =>'Since the first event in 2005 Fashion Week Stockholm has grown to become one of the most important fashion weeks in Northern Europe.',
    'post_parent' => 0
  );
  create_page($about_us_page);

  // Partners
  $partners = array(
    'post_title' => 'Partners',
    'post_content' =>'Fashion week partners',
    'post_parent' => 0
  );
  create_page($partners);

}

// Create default pages
add_action('after_switch_theme', __NAMESPACE__ . '\\generate_default_content');
