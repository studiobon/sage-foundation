<?php

namespace Roots\Sage\ACF;

/**
 * Advanced Custom Fields
 */


//
// Designers -> Collections
//

// Link Designer page to a new Collection page
// with a prepopulated Designers field
function get_collections_and_set_designer_id($field) {
  global $post;
  $field['message'] = '';
  $collections = get_collections( get_the_ID() );
  if($collections) {
    $field['message'] .= '<ul>';
    foreach( $collections as $collection ){
      $field['message'] .= '<li><a href="' . get_edit_post_link( $collection->ID ) . '">' . get_the_title( $collection->ID ) . '</a></li>';
    }
    $field['message'] .= '</ul>';
  }
  $field['message'] .= '<a class="acf-button blue" href="post-new.php?post_type=collections&designer_id='.$post->ID.'">Add a collection</a>';
  return $field;
}
add_filter('acf/load_field/key=field_564b4d9128af9', __NAMESPACE__ . '\\get_collections_and_set_designer_id', 10, 3);

// Add Designer to Collections admin columns
function add_collections_columns($columns) {
  $columns = array(
    'cb'    => '<input type="checkbox" />',
    'title'   => 'Season',
    'designers' => 'Designer',
    'date'    =>  'Date',
  );
  return $columns;
}
add_filter('manage_collections_posts_columns' , __NAMESPACE__ . '\\add_collections_columns');

function collection_custom_columns( $column ) {
  switch ( $column ) {
    case 'designers' :
      $posts = get_field('designer');
      if($posts) {
        foreach( $posts as $p ) {
          echo get_the_title( $p->ID );
        }
      }
      break;
  }
}
add_action('manage_collections_posts_custom_column', __NAMESPACE__ . '\\collection_custom_columns');

// Make Designer column sortable
function designer_column_register_sortable( $columns ) {
  $columns['designers'] = 'designers';
  return $columns;
}
add_filter('manage_edit-collections_sortable_columns', __NAMESPACE__ . '\\designer_column_register_sortable' );


//
// Designers -> Events
//

// Link Designer page to a new Collection page
// with a prepopulated Designers field
function get_events_and_set_designer_id($field) {
  global $post;
  $field['message'] = '';
  $events = get_events( get_the_ID() );
  if($events) {
    $field['message'] .= '<ul>';
    foreach( $events as $event ){
      $field['message'] .= '<li><a href="' . get_edit_post_link( $event->ID ) . '">' . get_the_title( $event->ID ) . '</a></li>';
    }
    $field['message'] .= '</ul>';
  }
  $field['message'] .= '<a class="acf-button blue" href="post-new.php?post_type=events&designer_id='.$post->ID.'">Add an event</a>';
  return $field;
}
add_filter('acf/load_field/key=field_564f4ad5372c2', __NAMESPACE__ . '\\get_events_and_set_designer_id', 10, 3);



// Check if designer_id query parameter
// is set in Collections or Events page.
// If so, select it from designer menu
function get_designer_id( $value, $post_id, $field ) {
  if(isset($_GET['designer_id'])) {
    $value = intval($_GET['designer_id']);
  }
  return $value;
}
add_filter('acf/load_value/name=designer', __NAMESPACE__ . '\\get_designer_id', 10, 3);
